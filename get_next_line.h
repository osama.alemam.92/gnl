#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

typedef struct strings{
    char    *line;
    char    *remain;
    int     *i;
}string;

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 42
# endif

char    *get_next_line(int fd);
string  str_cut(char *str);
char    *read_all(int fd, char *str);
char    *str_cat(char *str1, char *str2);
int     ft_strlen(const char *str);
int     *intiate_arr(int *arr);
int     check_nl(char *buff);


#endif
