#include "get_next_line.h"

char *str_cat(char *str1, char *str2)
{
    int i;
    int x;
    int total;
    char *res;

    i = 0;
    x = 0;
    total = ft_strlen(str1) + ft_strlen(str2);
    res = (char *)malloc(total + 1);
    if (!res || !str1 || !str2)
        return (0);
    while (str1[i] != '\0')
    {
        res[i] = str1[i];
        i++;
    }
    while (str2[x] != '\0')
        res[i++] = str2[x++];
    res[i] = '\0';
    if (str1)
       free (str1);
    return (res);
}

char *read_all(int fd, char *str)
{
    int r;
    char *buff;

    buff = (char *)malloc(BUFFER_SIZE + 1);
    while ((r = read(fd, buff, BUFFER_SIZE)) > 0)
    {
        buff[r] = '\0';
        str = str_cat(str, buff);
        if (check_nl(buff))
            break;
    }
    if (str[0] == '\0')
    {
        free (str);
        free (buff);
        return (NULL);
    }
    free (buff);
    return (str);
}

string str_cut(char *str)
{
    string res;

    res.i = 0;
    res.i = intiate_arr(res.i);
    while (str[res.i[0]] != '\n' && str[res.i[0]] != '\0')
        res.i[0]++;
    if (str[res.i[0]] == '\n')
        res.i[0]++;
    res.line = (char *)malloc(res.i[0] + 1);
    while (res.i[1] < res.i[0])
        res.line[res.i[2]++] = str[res.i[1]++];
    res.line[res.i[2]] = '\0';
    res.i[1] = res.i[2]-- + ft_strlen(str + res.i[1]);
    if (res.i[1] - res.i[2]++ > 0)
    {
        res.remain = (char *)malloc(res.i[1] - res.i[2] + 1);
        while (res.i[2] < res.i[1] && str[res.i[2]] != '\0')
            res.remain[res.i[3]++] = str[res.i[2]++];
        res.remain[res.i[3]] = '\0';
    }
    else
        res.remain = NULL;
    free (str);
    free (res.i);
    return (res);
}

char *get_next_line(int fd)
{
    static char *resdiue[16];
    char *line;
    string res;

    if (fd < 0 || BUFFER_SIZE <= 0 || read(fd, 0, 0) < 0)
		return (NULL);
    if (!resdiue[fd])
    {
        resdiue[fd] = (char *)malloc(1);
        resdiue[fd][0] = '\0';
    }
    resdiue[fd] = read_all(fd, resdiue[fd]);
    if (!resdiue[fd])
        return (NULL);
    res = str_cut(resdiue[fd]);
    line = res.line;
    resdiue[fd] = res.remain;
    return(line);
}