#include "get_next_line.h"

int check_nl(char *buff)
{
    int i;

    i = 0;
    while (i < BUFFER_SIZE && buff[i] != '\0')
    {
        if (buff[i] == '\n')
            return (1);
        i++;
    }
    return (0);
}

int *intiate_arr(int *arr)
{
    int i;

    i = 0;
    arr = malloc (4 * sizeof(int));
    while (i <= 3)
        arr[i++] = 0;
    return (arr);
}

int ft_strlen(const char *str)
{
	int i;

	i = 0;
	if (!str)
		return (0);
	while(str[i] != '\0' )
		i++;
	return(i);
}